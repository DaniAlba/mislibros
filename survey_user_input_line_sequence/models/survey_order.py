# -*- coding: utf-8 -*-

from openerp import models, fields, api


class SurveyUserIntputLine(models.Model):
    _inherit = 'survey.user_input_line'
    _order = "sequence asc"

    sequence = fields.Integer(string="Sequence", default="99999")
