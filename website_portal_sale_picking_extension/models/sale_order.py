# -*- coding: utf-8 -*-

from openerp import models, fields, api


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def line_webstate_calculate(self):
        for rec in self:
            for line in rec.order_line:
                line.web_state_calculate()
        return self

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    web_state = fields.Selection(selection=[
        ('draft', 'Pedido a editorial'),
        ('kept', 'Reservado'),
        ('packed', 'Empaquetado'),
        ('prepared', 'Preparado'),
        ('partial', 'Parcial'),
        ('done', 'Entregado'),
        ('cancel', 'Cancelado')],
        string="Estado en la web", default='draft')

    @api.multi
    def web_state_calculate(self):
        for rec in self:

            out_picking_ids = []
            pack_picking_ids = []
            pick_picking_ids = []


            product_id = rec.product_id
            if rec.order_id.state == 'progress':
                rec.web_state = "done"
            for procurement_id in rec.procurement_ids:

                done = []
                prepared = []
                packed = []
                kept = []
                cancel = []

                for move_id in procurement_id.move_ids:
                    # incluyo todos los objetos picking_id de esta linea de venta en una unica lista
                    group_id = move_id.group_id

                stock_move_ids = self.env['stock.move'].search([('group_id', '=', group_id.id), ('product_id', '=', product_id.id)])
                for move_id in stock_move_ids:

                    if "out" in move_id.picking_id.name.lower():
                        out_picking_ids.append(move_id.picking_id.name)
                        if move_id.state == 'done':
                            done.append(True)
                        else:
                            done.append(False)
                        if move_id.state == 'cancel':
                            cancel.append(True)
                        else:
                            cancel.append(False)

                    if "pick" in move_id.picking_id.name.lower():
                        pick_picking_ids.append(move_id.picking_id.name)
                        if move_id.state == 'done':
                            packed.append(True)
                        else:
                            packed.append(False)
                        if move_id.state == 'assigned':
                            kept.append(True)
                        else:
                            kept.append(False)
                        if move_id.state == 'cancel':
                            cancel.append(True)
                        else:
                            cancel.append(False)

                    if "pack" in move_id.picking_id.name.lower():
                        pack_picking_ids.append(move_id.picking_id.name)
                        if move_id.state == 'done':
                            prepared.append(True)
                        else:
                            prepared.append(False)
                        if move_id.state == 'cancel':
                            cancel.append(True)
                        else:
                            cancel.append(False)

                if done and all(done):
                    # pass
                    rec.web_state = "done"
                elif prepared and all(prepared):
                    # pass
                    rec.web_state = "prepared"
                elif packed and all(packed):
                    # pass
                    rec.web_state = "packed"
                elif packed and True in packed:
                    rec.web_state = "partial"

                elif kept and all(kept):
                    # pass
                    # SEGUN CARLOS TODOS SE QUEDAN EN ESTE ESTADO
                    rec.web_state = "kept"
                elif cancel and all(cancel):
                    # pass
                    rec.web_state = "cancel"
                else:
                    # pass
                    rec.web_state = "draft"

        return self

    @api.multi
    def lines_web_state_calculate(self):
        sale_order_lines_ids = self.env['sale.order.line'].search(['|', ('web_state', '!=', 'cancel'), ('web_state', '!=', 'done')])
        print "\n\n\n" + str(sale_order_lines_ids)
        for lines in sale_order_lines_ids:
            print lines
            lines.web_state_calculate()

    @api.model
    def cron_web_state_calculate(self):
        self.lines_web_state_calculate()


# v - Pedido a editorial (cuando su correspondiente linea en el albarán PICK está esperando disponibilidad) (Este sería el estado inicial, por defecto)
# v - Reservado (cuando su correspondiente linea en el albarán PICK ha sido reservada (asignado QUANT)
# V - Empaquetado (cuando su correspondiente linea en el albarán PICK ha sido realizado).
# V - Preparado (cuando su correspondiente linea en el albarán PACK ha sido realizado)
# V - Entregado (cuando su correspondiente linea en el albarán OUT ha sido realizado)
# V - Cancelado (cuando su correspondiente linea en el albarán PICK ha sido cancelado)

#   - Parcial (cuando es envio parcial)